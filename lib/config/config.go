package config

type ProviderSpecific interface{}

type Common struct{}

type Vessel struct {
	common           Common
	providerSpecific ProviderSpecific
}

type AWS ProviderSpecific
type GCP ProviderSpecific
type Azure ProviderSpecific
