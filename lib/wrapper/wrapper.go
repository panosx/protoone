package wrapper

// Name is the enum equivalent type for wrapper names
type Name int

const (
	// Could start from zero
	// but felt better to start from 1
	BOSH Name = 1 + iota
	Terraform
)

var Names = []string{
	"BOSH",
	"Terraform",
}

// String() function will return the english name
// of the constant Name
func (p Name) String() string {
	return Names[p-1]
}
