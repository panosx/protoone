package provider

// Name is the enum equivalent type for provider names
type Name int

const (
	// Could start from zero
	// but felt better to start from 1
	AWS Name = 1 + iota
	GCP
	Azure
)

var Names = []string{
	"AWS",
	"GCP",
	"Azure",
}

// String() function will return the english name
// of the constant Name
func (p Name) String() string {
	return Names[p-1]
}
