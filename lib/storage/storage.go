package storage

// Name is the enum equivalent type for storage names
type Name int

const (
	// Could start from zero
	// but felt better to start from 1
	S3 Name = 1 + iota
	GCS
)

var Names = []string{
	"S3",
	"GCS",
}

// String() function will return the english name
// of the constant Name
func (p Name) String() string {
	return Names[p-1]
}
