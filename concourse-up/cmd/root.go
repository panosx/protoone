package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "concourse-up",
}

func Execute() {
	rootCmd.AddCommand(deployCmd, destroyCmd, infoCmd, maintainCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}