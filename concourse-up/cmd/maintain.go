package cmd

import "github.com/spf13/cobra"

var maintainCmd = &cobra.Command{
	Use:   "maintain [deployment name]",
	Short: "Run maintenance actions against a named concourse",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

