package cmd

import "github.com/spf13/cobra"

var destroyCmd = &cobra.Command{
	Use:   "destroy [deployment name]",
	Short: "Destroy a named concourse",
	Run: func(cmd *cobra.Command, args []string) {
	},
}
