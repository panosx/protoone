package cmd

import "github.com/spf13/cobra"

var infoCmd = &cobra.Command{
	Use:   "info [deployment name]",
	Short: "Show deployment information about a named concourse",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

