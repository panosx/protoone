package main

import (
	"protoone/concourse-up/cmd"
)

func main() {
	cmd.Execute()
}
