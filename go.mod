module protoone

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/sys v0.0.0-20181205085412-a5c9d58dba9a // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
